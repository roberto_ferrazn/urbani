//
//  AppDelegate.h
//  Urbani
//
//  Created by Roberto Ferraz on 05/09/14.
//  Copyright (c) 2014 Roberto Ferraz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
